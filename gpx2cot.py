#!/usr/bin/python -u
# GPX player to Cursor-on-target (CoT)
# Copyright (c) 2020 by Alec Murphy, MIT licensed
#   https://gitlab.com/almurphy/
#
# Reads one or more GPS tracks in timestamped GPX format
#  and replays them at original speed into a CoT network

import logging
import os
import sys
import socket
import uuid
import re
import xml.etree.ElementTree as ET
from time import time, gmtime, strftime, strptime, mktime, sleep

# COT destination - default to TAK multicast network
ATAK_HOST = os.getenv('ATAK_HOST', '239.2.3.1')
ATAK_PORT = int(os.getenv('ATAK_PORT', '6969'))

# Enable debug output
DEBUG_LEVEL = int(os.getenv('DEBUG', '0'))

# How the actors identify on the network
BASE_NAME = os.getenv('BASE_NAME', 'ZOMBIE')   # callsign prefix
UNIT_TYPE = os.getenv('UNIT_TYPE', 'a-f-G-U-C')
UNIT_TEAM = os.getenv('UNIT_TEAM', 'Maroon')
UNIT_ROLE = os.getenv('UNIT_ROLE', 'Team Member')

# Play track(s) in infinite loop?
PLAY_LOOP = int(os.getenv('LOOP', '0'))

# Validity period of CoT events, for setting "stale" attribute
POINT_EVT_TTL = 120  # seconds

# Default altitude if none supplied in GPX
DEFAULT_ALT = "0.000000"

REMARK = 'GPX2COT'

ISO_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
TIME_FORMAT = '%Y-%m-%dT%H:%M:%S.000Z'


def parseTime(zuluTime):
    # remove milliseconds element if present
    if '.' in zuluTime:
        zuluTime = zuluTime.split('.')[0] + 'Z'
    return mktime(strptime(zuluTime, ISO_FORMAT))


def makeCoT(callsign, uuid, lat, lon, alt, addr='', port=''):
    """
    Generate a CoT XML object
    """

    # Event "how" (how the coordinates were generated)
    EV_HOW = 'm-g'  # GPS is how GPX files are usually made :)

    # Event fields
    event_attr = {
        'version': '2.0',
        'uid': uuid,
        'time': strftime(TIME_FORMAT, gmtime()),
        'start': strftime(TIME_FORMAT, gmtime()),
        'stale': strftime(TIME_FORMAT, gmtime(time() + POINT_EVT_TTL)),
        'type': UNIT_TYPE,
        'how': EV_HOW
    }

    # Point fields
    point_attr = {
        'lat': lat,
        'lon': lon,
        'hae': alt,
        'ce': '1.0',  # unspec
        'le': '9999999.0',  # unspec
    }

    contact_attr = { 'callsign': callsign }

    if len(addr) and len(port):
        contact_attr['endpoint'] = addr + ':' + port + ':tcp'

    precis_attr = { 'geopointsrc': 'GPS', 'altsrc': 'GPS' }

    takv_attr = {
        'device': 'GPX2COT',
        'platform': 'ATAK',
        'version': '3.8-COMPAT',
        'os': '23',
    }

    # Mandatory schema, "event" element at top level, with
    #   sub-elements "point" and "detail"
    cot = ET.Element('event', attrib=event_attr)
    ET.SubElement(cot, 'point', attrib=point_attr)
    det = ET.SubElement(cot, 'detail')

    ET.SubElement(det, 'contact', attrib=contact_attr)
    ET.SubElement(det, 'uid', attrib={'Droid': callsign})
    ET.SubElement(det, 'remarks').text = REMARK
    ET.SubElement(det, '__group', attrib={'name': UNIT_TEAM, 'role': UNIT_ROLE})

    # optional elements
    ET.SubElement(det, 'takv', attrib=takv_attr)
    # ET.SubElement(det, 'status', attrib={'battery': '100'})
    ET.SubElement(det, 'precisionlocation', attrib=precis_attr)

    cotXML = '<?xml version="1.0" standalone="yes"?>'.encode('utf-8')
    cotXML += ET.tostring(cot)

    return(cotXML)


if __name__ == '__main__':

    logger = logging.getLogger()
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.INFO)

    if DEBUG_LEVEL > 0:
        logger.setLevel(logging.DEBUG)

    # animated points moving along the given gpx tracks
    actors = {}

    for i in range(1, len(sys.argv)):
        logger.debug('Loading GPX file: ' + sys.argv[i] + '...')
        try:
            tree = ET.parse(sys.argv[i])
        except IOError:
            logger.error('ERROR: could not open file: ' + sys.argv[i])
            continue
        except ET.ParseError:
            logger.error('ERROR: invalid GPX format: ' + sys.argv[i])
            continue
        root = tree.getroot()

        actor = {}
        actor['id'] = i - 1
        actor['gpxfile'] = sys.argv[i]

        # Get the namespace from XML header - harder than it seems
        # actor['ns'] = root.get('targetNamespace', '')
        m = re.match(r'\{.*\}', root.tag)
        actor['ns'] = m.group(0) if m else ''
        logger.debug('  found root NS: ' + actor['ns'])

        # weird Python below - prepends namespace to each tag in an xpath
        xtrkpt = ('/' + actor['ns']).join('./trk/trkseg/trkpt'.split('/'))

        # load track points using findall with the xpath we built above
        actor['points'] = root.findall(xtrkpt)
        logger.debug('  found %d points' % len(actor['points']))

        # sanity check a few track points
        dataOK = True
        errMsg = ""
        for j in range(0, 5):
            try:
                point = actor['points'][j]
                # do the track points include timestamps?
                ptime = point.find(actor['ns'] + 'time').text
                if parseTime(ptime) > 0:
                    # Good timestamp
                    continue
            except:  # quite a lot of things can go wrong here
                dataOK = False
                logger.debug('  exception reading timestamps')
                errMsg = "missing timestamps"
                break

        if not dataOK:
            logger.error("GPX file: " + sys.argv[i] + " not OK: " + errMsg)
            actor = {}
            continue

        actor['uuid'] = str(uuid.uuid4())  # Fully random UUID
        actor['callsign'] = BASE_NAME + '-%02d' % actor['id']
        actor['battery'] = 96  # battery level (0-100), for extra realism
        actor['curpos'] = 0

        actors[actor['uuid']] = actor

    if len(actors):
        logger.info('running with %d actors' % len(actors))
    else:
        logger.error('No tracks could be loaded, exiting')
        exit()

    while True:  # MAIN LOOP
        if len(actors) == 0:
            logger.info('all tracks completed, exiting')
            break

        deleted = []

        for k in actors.keys():
            actor = actors[k]
            t_pos = actor['curpos']
            point = actor['points'][t_pos]

            ptime = point.find(actor['ns'] + 'time').text

            if DEBUG_LEVEL > 0:
                REMARK = ptime  # diiiirty

            trk_time = parseTime(ptime)

            # Time diff between present time and track timeline
            if ('timediff' not in actor):
                actor['timediff'] = time() - trk_time

            # Do nothing until it's time for next point
            if (time() < trk_time + actor['timediff']):
                continue

            # alt is optional, some GPXs don't have it but CoT needs it
            p_alt = DEFAULT_ALT  # sane default and go from there

            try:
                p_alt = point.find(actor['ns'] + 'ele').text
            except AttributeError:
                pass

            # It's time to send a position update
            # - construct CoT message from recorded data
            cotMsg = makeCoT(actor['callsign'], actor['uuid'],
                             point.attrib['lat'],
                             point.attrib['lon'], p_alt)

            # DEBUG DUMP - CoT message
            logger.debug('CoT: ' + cotMsg.decode('utf-8'))

            # Send CoT message to ATAK
            o_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            o_sock.sendto(cotMsg, (ATAK_HOST, ATAK_PORT))
            o_sock.close()
            logger.debug('Sent to %s:%d\n' % (ATAK_HOST, ATAK_PORT))

            # Advance current position, rollover if necessary
            t_pos += 1

            if t_pos >= len(actor['points']):
                if PLAY_LOOP:
                    logger.debug('Actor %s finished, restarting' %
                                 actor['callsign'])
                    t_pos = 0
                    # Delete timediff to force a reset (see above)
                    del actors[k]['timediff']
                else:
                    logger.debug('Actor %s finished, removing' %
                                 actor['callsign'])
                    # Remove from list
                    deleted.append(k)
                    continue

            actors[k]['curpos'] = t_pos

        for k in deleted:
            del actors[k]
            logger.debug('%d actors remaining' % len(actors))

        # Loop delay
        sleep(0.5)
